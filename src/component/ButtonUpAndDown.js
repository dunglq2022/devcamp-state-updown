import { Component } from "react";

class ButtuonUpAndDown extends Component{
    constructor(props){
        super(props)

        this.state = {
            count : 0
        }
    }

    buttonTang = () => {
        this.setState({
            count : this.state.count + 1
        })
    }

    buttonGiam = () => {
        this.setState({
            count : this.state.count - 1
        })
    }

    render() {
        return(
            <div>
                <p style={{fontSize: 30}}>Count: {this.state.count}</p>
                <button onClick={this.buttonTang} className="btn btn-primary btn-lg">TĂNG SỐ</button>
                <button onClick={this.buttonGiam} className="btn btn-danger btn-lg">GIẢM SỐ</button>
            </div>
        )
    }
}
export default ButtuonUpAndDown;